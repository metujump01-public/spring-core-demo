package hu.metujump.books.config;

import hu.metujump.books.annotation.DataAccessObject;
import hu.metujump.books.repository.DummyBookDao;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class ReflectionApiDemoTest {

    @Test
    public void annotationOnType() {

        Class type = FileBookAppConfig.class;

        final Configuration configAnnot = (Configuration) type.getAnnotation(Configuration.class);

        assertNotNull(configAnnot);

        final String configValue = configAnnot.value();

        assertEquals("fileConfig", configValue);

    }

    @Test
    public void annotationOnMethod() throws NoSuchMethodException {

        Class type = FileBookAppConfig.class;

        final Method fileBookDaoMethod = type.getMethod("fileBookDao");

        final Bean beanAnnotation = fileBookDaoMethod.getAnnotation(Bean.class);

        assertNotNull(beanAnnotation);

        final String[] name = beanAnnotation.name();

        final String[] expected = {"ez"};

        assertArrayEquals(expected, name);

    }

    @Test
    public void listOfAllMethods() throws NoSuchMethodException {

        Class type = FileBookAppConfig.class;

        for (Method method : type.getMethods()) {
            System.out.println(method);
        }

    }

    @Test
    public void testDataAccessObjectAnnotation() throws NoSuchMethodException {

        Class type = DummyBookDao.class;

        for (Annotation annotation : type.getAnnotations()) {
            System.out.println(annotation);
        }

        assertTrue(type.isAnnotationPresent(DataAccessObject.class));

    }

}