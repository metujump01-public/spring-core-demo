package hu.metujump.books;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotSame;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestPrototype.MyConfig.class)
public class TestPrototype {

    @Autowired
    private Book book1;
    @Autowired
    private Book book2;
    @Autowired
    private Book book3;

    @Autowired
    private ApplicationContext context;

    @Configuration
    static class MyConfig {

        @Scope("prototype")
        @Bean
        public Book createBook() {
            return new Book("title" + Math.random(),"author1" + Math.random());
        }
    }

    @Test
    void testPrototypeScope() {
        assertNotSame(book1, book2);
        assertNotSame(book1, book3);
        assertNotSame(book2, book3);
    }

    @Test
    void testContextGetBean() {
        final Book b1 = context.getBean(Book.class);
        final Book b2 = context.getBean(Book.class);

        assertNotSame(b1, b2);
    }

}
