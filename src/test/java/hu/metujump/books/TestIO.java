package hu.metujump.books;

import org.junit.jupiter.api.Test;

import java.io.*;

public class TestIO {

    @Test
    void readBytes() throws IOException {
        try (InputStream in = new FileInputStream("titles.txt")) {
            int readByte;

            while ((readByte = in.read()) != -1) {
                System.out.println("Read a byte: " + readByte);
            }
        }
    }

    @Test
    void rearCharactersFromFile() throws IOException {
        try (InputStream in = new FileInputStream("titles.txt")) {
            readCharacters(in);
        }
    }

    @Test
    void rearCharactersFromClassLoader() throws IOException {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("cimek.txt")) {
            readCharacters(in);
        }
    }

    @Test
    void rearLinesFromClassLoader() throws IOException {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("cimek.txt")) {
            readLines(in);
        }
    }

    private void readCharacters(InputStream in) throws IOException {
        InputStreamReader reader = new InputStreamReader(in, "UTF-8");
        int readChar;
        while ((readChar = reader.read()) != -1) {
            System.out.println("Read a char: " + (char)readChar);
        }
    }

    private void readLines(InputStream in) throws IOException {
        InputStreamReader reader = new InputStreamReader(in, "UTF-8");
        final BufferedReader br = new BufferedReader(reader);

        String readLine;
        while ((readLine = br.readLine()) != null) {
            System.out.println("Read a line: " + readLine);
        }
    }
}
