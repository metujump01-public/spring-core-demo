package hu.metujump.books;

import hu.metujump.books.repository.DummyBookDao;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;

public class DummyBookDaoTest {

    private DummyBookDao dummyBookDao;

    private static AtomicLong counter = new AtomicLong();

    private final long id;

    public DummyBookDaoTest() {
        id = counter.incrementAndGet();
        System.out.println("DummyBookDaoTestCreated: #" + id);
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("BeforeAll");
    }
    @AfterAll
    static void afterAll() {
        System.out.println("AfterAll");
    }

    @BeforeEach
    void beforeEach() {
        System.out.println("Before each #" + id);
        dummyBookDao = new DummyBookDao();
    }

    @AfterEach
    void cleanUp() {
        dummyBookDao = null;
        System.out.println("Cleaning up #" + id );
    }

    @Test
    @DisplayName("getTitles should not return a null value.")
    void testGetTitlesReturnsNotNull() {
        System.out.println("executing testGetTitlesReturnsNotNull on #" + id);
        // WHEN
        final List<String> titles = dummyBookDao.getTitles();

        // THEN
        assertNotNull(titles);
    }

    @Test
    @DisplayName("getTitles should return a list with 3 elements.")
    void testGetTitlesReturnsWithAListSized3() {
        System.out.println("executing testGetTitlesReturnsWithAListSized3 on #" + id);
        // WHEN
        final List<String> titles = dummyBookDao.getTitles();

        // THEN
        assertEquals(3, titles.size());
    }

}