package hu.metujump.books;

import hu.metujump.books.repository.BookDao;
import hu.metujump.books.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {

    @Bean
    public BookDao mockBookDao() {
        return mock(BookDao.class);
    }

    @Bean
    public BookService bookService(BookDao bookDao, ApplicationContext context) {
        return new BookService(bookDao, context);
    }
}
