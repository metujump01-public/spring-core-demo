package hu.metujump.books;

import hu.metujump.books.repository.BookDao;
import hu.metujump.books.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
public class BookServiceIntegrationTest {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookDao bookDao;

    @BeforeEach
    void beforeEach() {
        reset(bookDao);
    }

    @Test
    //@DirtiesContext()
    void getTitles() {
        //GIVEN
        when(bookDao.getTitles())
                .thenReturn(List.of("Alma","Bela","Attila"));
        //WHEN
        final List<String> titles =
                bookService.getTitles(title -> title.startsWith("A"));

        //THEN
        assertIterableEquals(List.of("Alma","Attila"), titles);
    }

    @Test
//    @DirtiesContext
    void getTitles2() {
        //GIVEN
        when(bookDao.getTitles())
                .thenReturn(List.of("Alma","Bela","Attila"));
        //WHEN
        final List<String> titles =
                bookService.getTitles(title -> title.startsWith("A"));

        //THEN
        assertIterableEquals(List.of("Alma","Attila"), titles);
    }
}
