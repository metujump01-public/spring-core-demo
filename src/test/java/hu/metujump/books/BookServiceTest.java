package hu.metujump.books;

import hu.metujump.books.repository.BookDao;
import hu.metujump.books.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class BookServiceTest {

    @Mock
    private BookDao bookDao;
    @InjectMocks
    private BookService bookService;
    private List<String> mockTitles;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockTitles = List.of();
        when(bookDao.getTitles())
                .thenAnswer(invocationOnMock -> mockTitles);
    }

    @Test
    @DisplayName("getTitles should return with the returned list of bookDao.getTitles()")
    void getTitlesShouldReturnBookDAOGetTitles() {
        //WHEN
        final List<String> titles = bookService.getTitles();
        //THEN
        verify(bookDao).getTitles();
        assertSame(mockTitles, titles);
    }

    @Nested
    @DisplayName("getTitles(filter)")
    class GetTitlesWithFilter {
        Predicate<String>filter;
        @BeforeEach
        void beforeEach() {
            filter = mock(Predicate.class);
            mockTitles = List.of("FIRST","SECOND","THIRD");
        }

        @Test
        @DisplayName("should call bookDao.getTitles()")
        void shouldCallBookDao() {
            //WHEN
            final List<String> titles = bookService.getTitles(filter);
            //THEN
            verify(bookDao).getTitles();

        }
        @Test
        @DisplayName("should call filter.test for all elements of the unfiltered list")
        void shouldCallPredicateTest() {
            //WHEN
            final List<String> titles = bookService.getTitles(filter);
            //THEN
            verify(filter).test("FIRST");
            verify(filter).test("SECOND");
            verify(filter).test("THIRD");
            verifyNoMoreInteractions(filter);
        }


        @Test
        @DisplayName("should return the properly filtered list")
        void shouldReturnTheProperFilteredList() {
            when(filter.test("FIRST"))
                    .thenReturn(true);
            when(filter.test("THIRD"))
                    .thenReturn(true);

            //WHEN
            final List<String> titles = bookService.getTitles(filter);
            //THEN

            assertIterableEquals(List.of("FIRST","THIRD"), titles);

        }
        @Test
        @DisplayName("should return an empty list if filter answers false for all elements")
        void shouldReturnAnEmptyListIfFilterAnswersFalseForAll() {
            //WHEN
            final List<String> titles = bookService.getTitles(filter);
            //THEN
            assertIterableEquals(List.of(), titles);

        }

        @Test
        @DisplayName("should throw a NullPointerException with a message 'filter should not be null' if provided filter is null")
        void shouldThrowNPEIfFilterIsNull() {
            final NullPointerException npe = assertThrows(NullPointerException.class, () -> {
                bookService.getTitles(null);
            });
            assertEquals("filter should not be null", npe.getMessage());
        }
    }



    @Nested
    @DisplayName("When constructing with bookDAO = null")
    class ConstructingWithNull {

        private Throwable exception;

        @BeforeEach
        void beforeEach() {
            bookDao = null;
            try {
                bookService = new BookService(bookDao, null);
            } catch (Throwable ex) {
                this.exception = ex;
            }
        }

        @Test
        @DisplayName("should throw a NullPointerException")
        void shouldThrowExceptionIfInstantiatedWithNullBookDAO() {
            assertNotNull(exception);
            assertEquals(NullPointerException.class, exception.getClass());
        }

        @Test
        @DisplayName("message should contain 'bookDAO'")
        void shouldThrowExceptionIfInstantiatedWithNullBookDAOMessage() {
            assertNotNull(exception.getMessage());
            assertTrue(exception.getMessage().contains("bookDAO"),
                    "Message is: '" + exception.getMessage() + "'");
        }


    }

}