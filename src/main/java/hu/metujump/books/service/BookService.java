package hu.metujump.books.service;

import hu.metujump.books.repository.BookDao;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class BookService {

    private BookDao bookDao;
    private ApplicationContext applicationContext;

    public BookService(BookDao bookDao, ApplicationContext applicationContext) {
        Objects.requireNonNull(bookDao,"bookDAO is required");
        this.bookDao = bookDao;
        this.applicationContext =  applicationContext;
    }

    public List<String> getTitles() {
        return bookDao.getTitles();
    }

    public List<String> getTitles(Predicate<String> filter) {
        Objects.requireNonNull(filter, "filter should not be null");
        List<String>filteredTitles = new ArrayList<>();
        for (String title: bookDao.getTitles()) {
            final boolean shouldBeInTheFilteredList
                    = filter.test(title);
            if (shouldBeInTheFilteredList) {
                filteredTitles.add(title);
            }
        }
        return filteredTitles;
    }

    public LocalDateTime getCurrentTime() {
        return this.applicationContext.getBean(LocalDateTime.class);
    }
}
