package hu.metujump.books;

import java.util.function.Predicate;

public class PlayWithPredicate {
    public static void main(String[] args) {
        Predicate<String> filter = title -> title.startsWith("A");

        System.out.println(filter.test("Aladár"));
    }
}
