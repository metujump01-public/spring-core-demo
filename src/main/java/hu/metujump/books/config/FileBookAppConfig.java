package hu.metujump.books.config;

import hu.metujump.books.annotation.Dummy;
import hu.metujump.books.annotation.FileBased;
import hu.metujump.books.annotation.FilenameOfFileBookDao;
import hu.metujump.books.repository.BookDao;
import hu.metujump.books.repository.FileBookDao;
import hu.metujump.books.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration("fileConfig")
@ComponentScan({"hu.metujump.books.repository"})
public class FileBookAppConfig {

    @Bean
    @FilenameOfFileBookDao
    public String fileName() {
        return "titles.txt";
    }

    @Bean
    public BookService bookService(@FileBased BookDao bookDao, ApplicationContext applicationContext) {
        return new BookService(bookDao, applicationContext);
    }

}
