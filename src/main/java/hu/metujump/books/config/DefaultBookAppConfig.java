package hu.metujump.books.config;

import hu.metujump.books.annotation.Dummy;
import hu.metujump.books.annotation.FilenameOfFileBookDao;
import hu.metujump.books.repository.BookDao;
import hu.metujump.books.repository.DummyBookDao;
import hu.metujump.books.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.time.DateTimeException;
import java.time.LocalDateTime;

@Configuration
@ComponentScan({"hu.metujump.books.repository"})
public class DefaultBookAppConfig {

    @Bean
    @FilenameOfFileBookDao
    public String fileName() {
        return "titles.txt";
    }

    @Bean
    public BookService bookService(@Dummy BookDao bookDao, ApplicationContext applicationContext) {
        return new BookService(bookDao, applicationContext);
    }

    @Bean
    @Scope("prototype")
    public LocalDateTime currentTime() {
        return LocalDateTime.now();
    }

}
