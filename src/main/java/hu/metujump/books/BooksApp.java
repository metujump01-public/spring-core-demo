package hu.metujump.books;

import hu.metujump.books.config.DefaultBookAppConfig;
import hu.metujump.books.config.FileBookAppConfig;
import hu.metujump.books.service.BookService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BooksApp {
    public static void main(String[] args) throws InterruptedException {

        Class configClass = DefaultBookAppConfig.class;

        if (args.length >0 && "-file".equals(args[0])) {
            configClass = FileBookAppConfig.class;
        }

        try(final AnnotationConfigApplicationContext context =
                    new AnnotationConfigApplicationContext(configClass)) {
            final BookService bookService = context.getBean( BookService.class);
            System.out.println("BookService.getTitles says: "
                    + bookService.getTitles(title -> title.startsWith("A")));
            System.out.println("BookService.getCurrentTime says: "
                    + bookService.getCurrentTime());
            Thread.sleep(1000);
            System.out.println("BookService.getCurrentTime says: "
                    + bookService.getCurrentTime());
        }
    }
}
