package hu.metujump.books.repository;

import hu.metujump.books.annotation.DataAccessObject;
import hu.metujump.books.annotation.FileBased;
import hu.metujump.books.annotation.FilenameOfFileBookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@DataAccessObject
@FileBased
public class FileBookDao implements BookDao {

    private String fileName;

    @Autowired
    public FileBookDao(@FilenameOfFileBookDao String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<String> getTitles() {
        final ArrayList<String> titles = new ArrayList<>();
        try (Scanner scanner = new Scanner(Paths.get(fileName))) {
            while (scanner.hasNext()) {
                titles.add(scanner.next());
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return titles;
    }
}
