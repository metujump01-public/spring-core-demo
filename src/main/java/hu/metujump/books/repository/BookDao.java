package hu.metujump.books.repository;

import java.util.List;

public interface BookDao {
    List<String> getTitles();
}
