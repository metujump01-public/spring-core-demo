package hu.metujump.books.repository;

import hu.metujump.books.annotation.DataAccessObject;
import hu.metujump.books.annotation.Dummy;
import java.util.List;

@Dummy @DataAccessObject
public class DummyBookDao implements BookDao {

    public DummyBookDao() {
    }

    @Override
    public List<String> getTitles() {
        return List.of("Egri csillagok", "Harry Potter", "Aranyember");
    }
}
